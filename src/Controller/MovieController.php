<?php


namespace App\Controller;


use App\Entity\Movie;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Interfaces\RouteCollectorInterface;
use Twig\Environment;

class MovieController
{
    public function __construct(
        private RouteCollectorInterface $routeCollector,
        private Environment $twig,
        private MovieRepository $movieRepository
    ) {}

    public function getMovie(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $movie = $this->getMovieById($args['id']);

        if (!$movie) {
            $response->getBody()->write($this->twig->render('404.html.twig'));

            return $response;
        }

        try {
            $data = $this->twig->render('movie/movie.html.twig', [
                'trailer' => $movie,
                'poster' => Movie::PAGE_POSTER_LOCATION
            ]);
        } catch (\Throwable $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }


        $response->getBody()->write($data);

        return $response;
    }

    private function getMovieById(string $id): ?Movie
    {
        return $this
            ->movieRepository
            ->find($id);
    }
}